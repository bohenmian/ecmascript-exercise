export default function countTypesNumber(source) {
  return Object.values(source).reduce((pre, curr) => pre + parseInt(curr, 10), 0);
}
