function fetchData(url) {
  return new Promise((resolve, reject) => {
    const xhr = new XMLHttpRequest();
    // <-- start
    xhr.onload(() => {
      resolve(xhr.responseText);
    });
    xhr.onerror(() => {
      reject(xhr.statusText);
    });
    xhr.open('post', url, true);
    // end -->
  });
}

const URL = 'http://localhost:3000/api';
fetchData(URL)
  .then(result => {
    document.writeln(JSON.parse(result).name);
  })
  .catch(error => {
    console.error(error);
  });
