async function fetchData(url) {
  // <-- start
  try {
    let result = await fetch(url);
    let data = await result.json();
    document.writeln(data.name);
  } catch (e) {
    document.writeln(e);
  }
  // end -->
}

const URL = 'http://localhost:3000/api';
fetchData(URL);
