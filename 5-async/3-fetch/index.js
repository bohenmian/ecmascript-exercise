function fetchData(url) {
  // <-- start
  fetch(url).then(function(response) {
    return response.json();
  });
  // end -->
}

const URL = 'http://localhost:3000/api';
fetchData(URL)
  .then(result => {
    document.writeln(result.name);
  })
  .catch(error => {
    console.error(error);
  });
