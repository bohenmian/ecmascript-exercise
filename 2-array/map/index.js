export default function numberMapToWord(collection) {
  return collection.map(c => String.fromCharCode(c + 96));
}
