function chooseMultiplesOfThree(collection) {
  return collection.filter(v => v % 3 === 0);
}

function chooseNoRepeatNumber(collection) {
  return Array.from(new Set(collection));
}

export { chooseMultiplesOfThree, chooseNoRepeatNumber };
