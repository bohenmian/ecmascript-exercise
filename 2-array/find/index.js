export default function find00OldPerson(collection) {
  return collection.filter(p => p.age <= 19 && p.age >= 9)[0].name;
}
