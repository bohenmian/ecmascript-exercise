export default function collectCarNumberCount(collection) {
  return collection.filter(word => word.startsWith('粤A')).length;
}
